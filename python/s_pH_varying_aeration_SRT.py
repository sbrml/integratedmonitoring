# vim: set tabstop=4
# s_DO_features_on_dataset_syntdata.py
#!/usr/bin/env python3
""" The script makes plots to classify data based on if the aeration ramp
freature. This is evaluate for a list of data and the result plotted."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 05.07.2018

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_O2 import aeration_ramp
from sbrfeatures.features_pH import aeration_valley

# user
from SBRplots.point_features import (
    feature_vs_value,
    feature_signal_and_output
)
from SBRdata.utils import (
    accuracy,
    contingencytable,
    fscore,
    processrawdata,
    Result,
    sensorsdict,
)
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 8})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# Configure the size and position of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3), loc='lower left',
                 mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])

def applyfeatureDO(time, svalue, *, low, sname):
    """
    Apply a feature to a signal.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.

    Returns
    -------
    float, array_like
        Time of feature occurance.
    bool, array_like
        Indicating if the feature has been found (True) or not (False).
    tuple, array_like
        Classification based on control measurements and the occurance of the
        feature.
    """
    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)
    _time = np.array([None] * N)

    for i, signal in enumerate(svalue): # signal are all DO values of one cycle
        _time[i] = np.array(time[i])/time[i][-1]
        tv, sv, ss = aeration_ramp(
            _time[i], signal, minslope=np.tan(60 * np.pi / 180),
            t_interval=timeminmax(_time[i][-1]), smoother=partial(
                smooth, order=2, freq=frequencyfkt(_time[i][-1])
                )
            )

        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], low[i])


    return time_feature, hasfeature, case

def applyfeaturepH(time, svalue, *, low, sname):
    """
    Apply a feature to a signal.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.

    Returns
    -------
    float, array_like
        Time of feature occurance.
    bool, array_like
        Indicating if the feature has been found (True) or not (False).
    tuple, array_like
        Classification based on control measurements and the occurance of the
        feature.
    """
    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)

    for i, signal in enumerate(svalue): # signal are all pH values of one cycle
        tv, sv, ss = aeration_valley(np.array(time[i]), signal,
                                     t_interval=timeminmax(time[i][-1]),
                                     smoother=partial(smooth, order=2,
                                         freq=frequencyfkt(time[i][-1])))

        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], low[i])

    return time_feature, hasfeature, case

def frequencyfkt(length=None):
    """ Sets the cutoff frequency parameter depending on the length of the
    cycle. If length is None, the assumption is made that the time is
    normalised in % of cycle passed, so from 0 to 1."""
    if length is None:
        freq = 2.0
    else:
        freq = 2.0 / length
    return freq

def timeminmax(length=None):
    """ Sets the range at the beginning and at the end of a cycle, where a
    local minimum is not considered to be a relevant feature, when falling into
    that range. This margine depends on the length of the cycle.
    If length is None, the assumption is made that the time is normalised in %
    of cycle passed, so from 0 to 1."""
    if length is None:
        [0.1, 0.9]
    else:
        t_int_from = 0.1 * length
        t_int_to = 0.99 * length
    return [t_int_from, t_int_to]

def createtf(inputparam, *, sensortype="DO"):
    ''' DO and pH can be evaluated for  different sludge ages (also
    called solids retention time, in short SRT). This goes through all
    different SRTs.
    '''
    TFtable = {}
    hasfeature = {}
    time_feature = {}
    cases = {}
    accuracytable = {}
    for input in inputparam:
        sensorsource = {input : DATAPATH + "su_%s_%s.csv" % (sensortype, input)}
        NH4, nonan, scenario = processrawdata(filesdict=sensorsource)
        # sets the threshold for the decision of a cycle is labled as positive
        # or negative.
        NH4ISLOW = (NH4 <= NH4THRESHOLD)

        # compare prediction with the measured value.
        for sname, signal in scenario.items():
            print('** Scenario type: %s' %sname)

            CYCLENAMES = np.array(signal.cycle_names())[nonan]
            ## Filter cycles which do not have an ammonium reference measurement
            ## and select aeration phase (phase 4)
            _time, _sv = signal.select_phase(phase=4)
            _svalue = _sv[nonan, :]
            value = signal.interp_nan(_time, _svalue.copy())

            svalue = []
            time = []
            i = 0
            for line in value:
                 sv = line[0:signal.last_valid_idx[i]]
                 st = [10.0*x for x in range(0, signal.last_valid_idx[i])]
                 svalue.append(sv)
                 time.append(st)
                 i += 1
            if sensortype == 'DO':
                time_feature[sname], hasfeature[sname], cases[sname] = applyfeatureDO(
                    time, svalue, low=NH4ISLOW, sname=sname)

            if sensortype == 'pH':
                time_feature[sname], hasfeature[sname], cases[sname] = applyfeaturepH(
                    time, svalue, low=NH4ISLOW, sname=sname)

            ## creates the tables to save the results
            TFtable[sname] = contingencytable(cases[sname])
            accuracytable[sname] = accuracy(TFtable[sname])

            ## Print summary of classification, number of TP, TN, FP and FN,
            ## f-score, and soft sensor accuracy.
            for k, v in TFtable[sname].items():
                print('{}:\t{}\t({})'.format(Result[k].value.text, v, k))
            for beta in [0.5, 1, 2]:
                print('F({:.1f})-score: {:.2f}'.format(
                    beta, fscore(TFtable[sname], beta**2)))
            print('Accuracy: {:.2f}'.format(accuracy(TFtable[sname])))
    return TFtable, accuracytable

if __name__ == '__main__':
    from  functools import partial

    DATAPATH = "../data/manuscript/"
    inputparam = ["025Nl_SRT02d", "050Nl_SRT02d", "100Nl_SRT02d", "150Nl_SRT02d",
                  "200Nl_SRT02d", "250Nl_SRT02d", "300Nl_SRT02d",
                  "025Nl_SRT05d", "050Nl_SRT05d", "100Nl_SRT05d", "150Nl_SRT05d",
                  "200Nl_SRT05d", "250Nl_SRT05d", "300Nl_SRT05d",
                  "025Nl_SRT10d", "050Nl_SRT10d", "100Nl_SRT10d", "150Nl_SRT10d",
                  "200Nl_SRT10d", "250Nl_SRT10d", "300Nl_SRT10d",
                  "025Nl_SRT15d", "050Nl_SRT15d", "100Nl_SRT15d", "150Nl_SRT15d",
                  "200Nl_SRT15d", "250Nl_SRT15d", "300Nl_SRT15d"]

    TFtablepH, accuracytablepH = createtf(inputparam, sensortype="pH")

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
    i2 = 0
    i5 = 0
    i10 = 0
    i15 = 0
    # plotting pH
    for name, value in accuracytablepH.items():
        # isolating the number in the filename
        _SRT = name.split('_')[1]
        SRT = int(''.join(s for s in _SRT if s.isdigit()))
        _air = name.split('_')[0]
        air = int(''.join(s for s in _air if s.isdigit()))

        # Treatment performance measured in ammonium nitrogen in the effluent
        allpos = (TFtablepH[name]["TRUE_POSITIVE"] + TFtablepH[name]["FALSE_NEGATIVE"]) \
            /(TFtablepH[name]["TRUE_POSITIVE"] + TFtablepH[name]["FALSE_NEGATIVE"] \
            + TFtablepH[name]["TRUE_NEGATIVE"] + TFtablepH[name]["FALSE_POSITIVE"])

        if SRT == 5:
            if i5 == 0:
                ax.plot(air, value, marker='o', markersize=12, markerfacecolor="None",
                    alpha=1, markeredgecolor='#7bccc4', markeredgewidth=1.2,
                    label="SRT 5d", linestyle='None')
                ax.plot(air, allpos, marker='^', markersize=11, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#7bccc4', label="perf",
                        linestyle = 'None')
            if i5 is not 0:
                ax.plot(air, value, marker='o', markersize=12, markerfacecolor="None",
                    alpha=1, markeredgecolor='#7bccc4', markeredgewidth=1.2)
                ax.plot(air, allpos, marker='^', markersize=11, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#7bccc4')
            i5 += 1

        if SRT == 15:
            if i15 == 0:
                ax.plot(air, value, marker='o', markersize=6, markerfacecolor="None",
                    alpha=1, markeredgecolor='#0868ac', markeredgewidth=1.2,
                    label="SRT 15d", linestyle = 'None')
                ax.plot(air, allpos, marker='^', markersize=2, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#0868ac', label="perf",
                        linestyle = 'None')
            if i15 is not 0:
                ax.plot(air, value, marker='o', markersize=6,
                        markerfacecolor="None", alpha=1,
                        markeredgecolor='#0868ac', markeredgewidth=1.2)
                ax.plot(air, allpos, marker='^', markersize=2, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#0868ac')
            i15 += 1

        if SRT == 2:
            if i2 == 0:
                ax.plot(air, value, marker='o', markersize=16, markerfacecolor="None",
                    alpha=1, markeredgecolor='#bae4bc', markeredgewidth=1.2,
                    label="SRT 2d", linestyle='None')
                ax.plot(air, allpos, marker='^', markersize=4, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#bae4bc', label="perf",
                        linestyle = 'None')
            if i2 is not 0:
                ax.plot(air, value, marker='o', markersize=16, markerfacecolor="None",
                    alpha=1, markeredgecolor='#bae4bc', markeredgewidth=1.2)
                ax.plot(air, allpos, marker='^', markersize=4, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#bae4bc')
            i2 += 1

        if SRT == 10:
            if i10 == 0:
                ax.plot(air, value, marker='o', markersize=9, markerfacecolor="None",
                    alpha=1, markeredgecolor='#43a2ca', markeredgewidth=1.2,
                    label="SRT 10d", linestyle = 'None')
                ax.plot(air, allpos, marker='^', markersize=7, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#43a2ca', label="perf",
                        linestyle = 'None')
            if i10 is not 0:
                ax.plot(air, value, marker='o', markersize=9,
                        markerfacecolor="None", alpha=1,
                        markeredgecolor='#43a2ca', markeredgewidth=1.2)
                ax.plot(air, allpos, marker='^', markersize=7, markerfacecolor="None",
                        alpha=1,  markeredgewidth=1.2,
                        markeredgecolor='#43a2ca')
            i10 += 1

        # ax.plot(air, allpos, marker='^', markersize=5, c='white',
        #           alpha=1,  markeredgewidth=1.2, markeredgecolor='#253494')

    ax.set_xlabel('aeration rate (SL$min^{-1}$)')
    ax.set_ylabel('accuracy and treatment performance [-]')
    ax.legend(ncol=1, fontsize=10, loc=7)
    fig.tight_layout(pad=0.1)

    plt.show(block=True)

    TFdfpH = pd.DataFrame(TFtablepH.items())
    accuracydfpH = pd.DataFrame(accuracytablepH.items())
    df = pd.concat([TFdfpH, accuracydfpH], axis=1, sort=False)

    filename = DATAPATH + "result_synthetic_data.csv"
    df.to_csv(filename, sep=";")
