# vim: set tabstop=4
# s_cathegories.py
#!/usr/bin/env python3

# Copyright (C) 2019 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 12.08.2019

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# user
from SBRdata.utils import (
    Result,
    sensorsdict,
    processrawdata,
    fscore,
    contingencytable
)
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

CAT = np.array([])

def plotcategory(cat=1):
    # if category == cat
    pass
if __name__ == '__main__':
    sensorsource = sensorsdict(stype='DO')
    cat, nonan, sensor = processrawdata(filesdict=sensorsource, refname='Category')

    for sname, signal in sensor.items():
        print('**Sensor type: %s' %sname)
        sortedcycles = []
        i = 0
        CYCLENAMES = np.array(signal.cycle_names())
        time, svalue = signal.select_phase(phase=4)
        fig, ax = plt.subplots(1, 1)
        # svalue = signal.interp_nan(time, svalue)
        for line in svalue:
            if cat[i] == 1:
                sortedcycles.append(line)
                ax.plot(line)
            else:
                pass
            i += 1

        ax.set_title('False positive predictions for %s' %sname.replace('_', r' '))
        ax.set_xlabel('time in 10 s')
        # ax.set_ylabel('dissolved oxygen [mgO2/L]')
        ax.set_ylabel('dissolved oxygen in mgO2/L')
        # plt.plot(sortedcycles)
        plt.show()
