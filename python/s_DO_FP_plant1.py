# vim: set tabstop=4
# s_DO_FP_plant1.py
#!/usr/bin/env python3

""" This module plots a few exemplary cycles and the location of the features
detected.
"""

# Copyright (C) 2019 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 12.08.2019

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_O2 import aeration_ramp

# user
from SBRdata.utils import (
    sensorsdict,
    processrawdata
)
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 10})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

def frequencyfkt(length=None):
    """ Defines the cut-off frequency based on the total lenght of the aeration
    phase.
    """
    # import pdb; pdb.set_trace()
    if length is None:
        freq = 2.0
    else:
        freq = 2.5 / length
    return freq

def timeminmax(length=None):
    """ Removes features detected at the very beginnning or very end of the
    aeration phase, as not trustworthy.
    """
    if length is None:
        t_int_from = 0.1
        t_int_to = 0.9
    else:
        t_int_from = 0.2 * length
        t_int_to = 1 * length

    return (t_int_from, t_int_to)

if __name__ == '__main__':
    from functools import partial
    SENSORSOURCE = sensorsdict(stype='DO')
    CAT, _, SENSOR = processrawdata(filesdict=SENSORSOURCE,
                                    refname='Category')

    for sname, signal in SENSOR.items():
        # This skript is very specifict to plot a few visually picked
        # cycles of plant 1 to show certain behaviour of the dissolved
        # oxygen measurements. The color selection and labels cannot just be
        # used with other data.
        print('**Sensor type: %s' %sname)

        time_sorted_cycles = []
        i = 0
        ii = 0

        # gets the timestamp and the measurement of in 10 seconds intervals
        # for the aeration phase (phase 4) only.
        time, svalue = signal.select_phase(phase=4)

        # prepares the figure to be ploted in and sets the parameters for the
        # plot.
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
        color = ["deepskyblue", "deepskyblue", "deepskyblue", "limegreen",
                 "darkgreen"]
        label_added_feat = False
        FMT = {'va':'center', 'ha':'center', 'fontsize':8, 'color':"grey"}

        for line in svalue:
            # the cycles have a category, which is stored together with the
            # reference measurement values. The here chosen category is 8.
            if CAT[i] == 8:
                # last_valid_idx depends on the length of the aeration phase
                # and returns the index of the last valid value.
                index = signal.last_valid_idx[i]
                # creates the time stamps since beginning of the aeration
                # phase in hours.
                time_hours = np.arange(0, index*10/60/60, 10/60/60)

            # plots the measured dissolved oxygen values in 10 second intervals.
                if ii <= 2:
                    if not label_added_feat:
                        ax.plot(time_hours, line[0:index+1], color=color[ii],
                                alpha=0.5, label="false positive")
                    else:
                        ax.plot(time_hours, line[0:index+1], color=color[ii],
                                alpha=0.5)
                if ii == 3:
                    ax.plot(time_hours, line[0:index+1], color=color[ii],
                            alpha=0.5, label="true negative")
                if ii == 4:
                    ax.plot(time_hours, line[0:index+1], color=color[ii],
                            alpha=0.5, label="modelled true positive")

                # checks the presence of a feature or not and returns the
                # time of the feature detection, the slope at the place
                # where the feature was detected and the smoothed curve.
                tv, sv, ss = aeration_ramp(
                    time_hours, line[0:index+1],
                    minslope=np.tan(40 * np.pi / 180),
                    t_interval=timeminmax(index*10/60/60),
                    smoother=partial(smooth, order=2,
                                     freq=frequencyfkt(index*10/60/60))
                    )

                feature = (tv, sv, ss)

                # this is used to plot the place of the feature and the
                # slope in the smoothed siganl where the feature occures.
                if tv is not None:
                    # draw local slope line
                    tv = feature[0]
                    yv = feature[1][0]
                    dyv = feature[1][1]
                    dtloc = np.array([-0.3, 0.3])
                    slope_deg = np.real(np.arctan(dyv)) * 180 / np.pi
                    # plots the calculated slope
                    plt.annotate('{:.0f}°'.format(slope_deg), (tv + 0.5, yv),
                                 **FMT)

                    # to only have the label once in the legned.
                    if not label_added_feat:
                        ax.plot(time_hours, ss, '--', color=color[ii],
                                label="smoothed signal", alpha=0.6)
                        ax.plot(tv, sv[0], 'o', markerfacecolor="None",
                                markeredgecolor='grey',
                                label="feature")
                        ax.plot(tv + dtloc, yv + dyv * dtloc, 'grey',
                                label="slope")
                        label_added_feat = True

                    else:
                        ax.plot(time_hours, ss, '--', color=color[ii],
                                alpha=0.6)
                        ax.plot(tv, sv[0], 'o', markerfacecolor="None",
                                markeredgecolor='grey')
                        ax.plot(tv + dtloc, yv + dyv * dtloc, 'grey')
                else:
                    ax.plot(time_hours, ss, '--', color=color[ii],
                            alpha=0.6)
                ii += 1
            else:
                pass
            i += 1

        # set labels and legend of the figure
        ax.set_xlabel('time [hours]')
        ax.set_ylabel('dissolved oxygen [$g_{O_{2}}m^{-3}$]')
        LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3), loc='lower left',
                         mode='expand', ncol=3, fontsize=8, frameon=False)
        ax.legend(fontsize=8, frameon=False)
        # pad=0.0 ensures that a figure has no additional boder for a
        # publication, to be exactly of the same size.
        fig.tight_layout(pad=0.0)

        # Uncomment to save the figure
        # fig.savefig('test.png', dpi=300)

        plt.show()
