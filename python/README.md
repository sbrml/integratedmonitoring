# Batch reactor feature based regression adapted for SBRs in operation

Scripts to reproduce the analyses of batch reactors signals reported in the article [xxy](https://engrxiv.org/ndm7f/)

## Authors

* **Mariane Schneider**
* **Juan Pablo Carbajal**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    └── python

The `data` folder is meant to store raw data and the output of scripts.
It is actually a placeholder, no data should be added to the repository.
The data will be shared with other platforms.

The `doc` folder contains documents used during the development of this package.
They do not contain actual documentation; to get documentation please refer to the [xxy](https://engrxiv.org/ndm7f/).

The `python` folder contains scripts implementing the analysis of the data using the features from the [sbrfeatures](https://gitlab.com/kakila/sbrfeatures) module.
