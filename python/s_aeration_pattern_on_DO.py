# vim: set tabstop=4
# s_cathegories.py
#!/usr/bin/env python3

""" This module plots a few exemplary cycles and the location of the features
detected.
"""

# Copyright (C) 2019 Maraine Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 12.08.2019

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_O2 import aeration_ramp

# user
from SBRdata.utils import (
    sensorsdict,
    processrawdata
)
############
# Global setup of
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 8})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

def frequencyfkt(length=None):
    """ Defines the cut-off frequency based on the total lenght of the aeration
    phase.
    """
    if length is None:
        freq = 2.0
    else:
        freq = 2.5 / length
    return freq

def timeminmax(length=None):
    """ Removes features detected at the very beginnning or very end of the
    aeration phase, as not trustworthy.
    """
    if length is None:
        [0.1, 0.9]
    else:
        t_int_from = 0.2 * length
        t_int_to = 1 * length

    return (t_int_from, t_int_to)

if __name__ == '__main__':
    from functools import partial
    SENSORSOURCE = sensorsdict(stype='DO')
    # reads the rawdata for all files listed in SBRdata.utils.py, which are of
    # the sensortype DO.
    CAT, _, SENSOR = processrawdata(filesdict=SENSORSOURCE, refname='Category')

    for sname, signal in SENSOR.items():
        print('**Sensor type: %s' %sname)

        # Taking only the aeration phase
        time, svalue = signal.select_phase(phase=4)

        # Figure setup
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
        color = ["deepskyblue", "steelblue", "limegreen", "darkgreen",
                 "darkslategrey"]
        label = ["ideal", "delay", "timed on-off", "continuous", "pause"]
        i = 0
        FMT = {'va':'center', 'ha':'center', 'fontsize':8, 'color':"black"}
        for line in svalue:
            # Check how long the aeration phase is
            index = signal.last_valid_idx[i]
            # translate the amount of measured values into time (here for a
            # 10 seconds resolution)
            time_hours = np.arange(0, index*10/60/60, 10/60/60)

            # plot all data in the csv file.
            ax.plot(time_hours, line[0:index+1], color=color[int(CAT[i])],
                    alpha=0.5, label=label[int(CAT[i])])

            # find the aeration ramp feature in the data
            tv, sv, ss = aeration_ramp(
                time_hours, line[0:index+1],
                minslope=np.tan(40 * np.pi / 180),
                t_interval=timeminmax(index*10/60/60),
                smoother=partial(smooth, order=2,
                                 freq=frequencyfkt(index*10/60/60))
                )
            # Definition of where the feature is lying.
            feature = (tv, sv, ss)

            # plot the feature if it exists
            if tv is not None:
                tv = feature[0]
                yv = feature[1][0]
                dyv = feature[1][1]
                # defining the length of the displayed slope in the plot
                dtloc = np.array([-0.1, 0.1])
                # calculate the slope
                slope_deg = np.real(np.arctan(dyv)) * 180 / np.pi
                if int(CAT[i]) == 4:
                    ax.plot(tv, sv[0], 'o', color=color[int(CAT[i])],
                            markeredgecolor='black',
                            label="aeration ramp", alpha=0.6)
                else:
                    ax.plot(tv, sv[0], 'o', color=color[int(CAT[i])],
                            markeredgecolor='black', alpha=0.6)
                # shows a line with the lenght proportional to the slope.
                ax.plot(tv + dtloc, yv + dyv * dtloc, color='black', alpha=0.6)
                # displays the slope in °.
                plt.annotate('{:.0f}°'.format(slope_deg), (tv + -0.1, yv + 0.1),
                             **FMT)
            # plots the smoothed signal. If statement only for legend.
            if int(CAT[i]) == 4:
                ax.plot(time_hours, ss, '--', color=color[int(CAT[i])],
                        label="smoothed signal", alpha=0.6)
            else:
                ax.plot(time_hours, ss, '--', color=color[int(CAT[i])], alpha=0.6)
            i += 1

        ax.set_xlabel('time [hours]', fontsize=12)
        ax.set_ylabel('dissolved oxygen [$g_{O_{2}}$$m^{-3}$]', labelpad=-1.5,
                      fontsize=12)

        ax.legend(fontsize=8)
        plt.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)

        fig.savefig('figure01_integratedmonitoring.png', dpi=300)
        plt.show()
