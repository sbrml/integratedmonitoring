# vim: set tabstop=4
# utils.py
#!/usr/bin/env python3

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

############
## Imports
# 3rd party
from collections import namedtuple
from enum import Enum
import numpy as np

# user
from SBRdata.dataparser import(
    SensorData,
    IOData)
############

ENUM_VAL = namedtuple('Results_Enum_Value', ['fmt', 'text'])
class Result(Enum):
    '''
    False Positive: feature predicts condition (e.g. pH areation valley) but
                    doesn't have condition (e.g. NH4 above threshold)
    False Negative: feature doesn't predict condition (e.g. pH areation
                    valley not found) but have condition (e.g. NH4 below
                    threshold)
    True positive: feature predicts condition (e.g. pH areation valley) and
                   have condition (e.g. NH4 below threshold)
    True negative: feature doens't predict condition (e.g. pH areation valley
                   not found) and doesn't have condition (e.g. NH4 above threshold)
    '''
    FALSE_POSITIVE = ENUM_VAL(
        fmt=dict(color='r', marker='v', markerfacecolor='w', markersize=8),
        text='Expected v Got ^'
        )
    FALSE_NEGATIVE = ENUM_VAL(
        fmt=dict(color='r', marker='^', markerfacecolor='w', markersize=8),
        text='Expected ^ Got v'
        )
    TRUE_POSITIVE = ENUM_VAL(
        fmt=dict(color='g', marker='v', markerfacecolor='g', markersize=8),
        text='Expected v Got v'
        )
    TRUE_NEGATIVE = ENUM_VAL(
        fmt=dict(color='g', marker='^', markerfacecolor='g', markersize=8),
        text='Expected ^ Got ^'
        )

    @classmethod
    def classify(cls, hasfeature, hascondition):
        if hasfeature and hascondition:
            return cls.TRUE_POSITIVE
        elif (not hasfeature) and (not hascondition):
            return cls.TRUE_NEGATIVE
        elif hasfeature and (not hascondition):
            return cls.FALSE_POSITIVE
        elif (not hasfeature) and hascondition:
            return cls.FALSE_NEGATIVE

def fscore(table, beta2):
    ''' Calculation of the f-score with the beta as an input in order to give
        more or less weight to the FALSE_NEGATIVE or FALSE_POSITIVE.
    '''
    divisor = ((1 + beta2) * table['TRUE_POSITIVE']
       + beta2 * table['FALSE_NEGATIVE']
       + table['FALSE_POSITIVE'])
    try:
        z = (1 + beta2) * table['TRUE_POSITIVE'] \
            / ((1 + beta2) * table['TRUE_POSITIVE']
            + beta2 * table['FALSE_NEGATIVE']
            + table['FALSE_POSITIVE'])
    except ZeroDivisionError:
        z = 1
    return z

def accuracy(table):
    sumtrue = table['TRUE_POSITIVE'] + table['TRUE_NEGATIVE']
    total = sumtrue + table['FALSE_NEGATIVE'] + table['FALSE_POSITIVE']
    return sumtrue / total

def contingencytable(cases):
    table = dict().fromkeys([x.name for x in list(Result)], 0)
    for c in cases:
        table[c.name] += 1
    return table

SensorInfo = namedtuple('SensorInfo', ["maintained", "stype", "filename"])

# FIXME: do this system independent
DATAPATH = '../data/'
SENSORS = (
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"ba_pH01.csv"),
    # SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"ba_pH02.csv"),
    # SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"ba_pH03.csv"),
    SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"OST2_pH01.csv"),
    SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"OST2_pH02.csv"),
    SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"OST2_pH03.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"OST3_pH01.csv"),
    # SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"OST3_pH02.csv"),
    # SensorInfo(stype="pH", maintained=False, filename=DATAPATH+"OST3_pH03.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT10d.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT05d.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT02_5d.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_deni.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT10d_deni.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT05d_deni.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT02_5d_deni.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_deni2.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT10d_deni2.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT05d_deni2.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT02_5d_deni2.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_deni2_low.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT10d_deni2_low.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT05d_deni2_low.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT02_5d_deni2_low.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_deni2_low_l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT10d_deni2_low_l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT05d_deni2_low_l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT02_5d_deni2_low_l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_025l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_050l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_150l.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_0350ALK.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_0500ALK.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_SRT20d_0550ALK.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_0350ALK_longdeni.csv"),
    # SensorInfo(stype="pH", maintained=True, filename=DATAPATH+"su_pH_0500ALK_longdeni.csv"),
    SensorInfo(stype="ORP", maintained=True, filename=DATAPATH+"orp01.csv"),
    SensorInfo(stype="ORP", maintained=False, filename=DATAPATH+"orp02.csv"),
    SensorInfo(stype="ORP", maintained=False, filename=DATAPATH+"orp03.csv"),
    # SensorInfo(stype="DO", maintained=True, filename=DATAPATH+"OST2_do01.csv"),
    # SensorInfo(stype="DO", maintained=False, filename=DATAPATH+"OST2_do02.csv")
    # SensorInfo(stype="DO", maintained=True, filename=DATAPATH+"su_DO_SRT_03d.csv")
    # SensorInfo(stype="DO", maintained=True, filename=DATAPATH+"ba_do01.csv")
    # SensorInfo(stype="DO", maintained=True, filename=DATAPATH+"OST3_do01.csv"),
    # SensorInfo(stype="DO", maintained=False, filename=DATAPATH+"OST3_do02.csv")
    SensorInfo(stype="DO", maintained=False, filename=DATAPATH+"bs_do01.csv")
    )

def sensorsdict(*, stype=None, maintained=None):
    """ Creates a dictionary which returns all sensors of the given sensor
        type, categorises them in maintained and unmaintained sensors and
        linkes them with a location and a file name.

        Arguments
        ---------
        None

        Keyword arguments
        -----------------
        stype : string
            Defines which sensortype we are using. Sensor types can be added
            to SENSORS.
        maintained : boolean (True/False)
            Information if the sensor is maintained or not.

        Returns
        -------
        dictionary
            Sensor linked to location of the data file.

    """
    names = []
    files = []
    count = {'maintained':0, 'unmaintained':0}
    if (stype is None) and (maintained is None):
        for x in SENSORS:
            m = 'maintained' if x.maintained else 'unmaintained'
            if m in names:
                count[m] += 1
                m = m + str(count[m])
            names.append('{}:{}'.format(x.stype, m))
            files.append(x.filename)

    elif (stype is not None) and (maintained is None):
        for x in SENSORS:
            if x.stype == stype:
                m = 'maintained' if x.maintained else 'unmaintained'
                count[m] += 1
                m = m + '_' + str(count[m])
                names.append(m)
                files.append(x.filename)

    elif (stype is None) and (maintained is not None):
        for x in SENSORS:
            if x.maintained == maintained:
                m = 'maintained' if x.maintained else 'unmaintained'
                if x.stype in names:
                    count[m] += 1
                    m = x.stype + str(count[m])
                names.append(m)
                files.append(x.filename)

    elif (stype is not None) and (maintained is not None):
        for x in SENSORS:
            if (x.maintained == maintained) and (x.stype == stype):
                m = 'maintained' if x.maintained else 'unmaintained'
                if m in names:
                    count[m] += 1
                    m = m + str(count[m])
                names.append('{}:{}'.format(x.stype, m))
                files.append(x.filename)

    return dict(zip(names, files))

def processrawdata(*, filesdict, refname="NH4"):
    """
        Arguments
        ---------
        None

        Keyword arguments
        -----------------
        filesdict : dictionary
            Sensor linked to location of the data file.

        Returns
        -------
        array
            All reference measurements values from the input output data where
            one exists.
        array
            Returns a boolean for each cycle. True means that a reference
            measurement exists. False means that non exists, which indicates
            that this cycle is not going to be used.
        dictionary
            Containes sensorinformation and sensordata.
    """
    sensor = dict()
    for sname, filename in filesdict.items():
        print('Processing signal {} from {}'.format(sname, filename))
        # gets the sensordata from :func:`~dataparser.SensorData.__init__`
        sensor[sname] = SensorData(filename)

        # gets the input-output data from :func:`~dataparser.IOData.__init__`
        Ydata = IOData(filename)
        if refname == 'NH4':
            if 'NH4' not in locals():
                refmeas, nonan = Ydata.filter_missing('NH4', flowdir='out')
            else:
                # Check allfiles have the same output values
                tmp, _ = Ydata.filter_missing('NH4', flowdir='out')
                if not np.allclose(NH4, tmp):
                    raise ValueError('Output signals in %s differ!'%filename)

        else:
            refmeas, nonan = Ydata.filter_missing(refname)

    return refmeas, nonan, sensor
