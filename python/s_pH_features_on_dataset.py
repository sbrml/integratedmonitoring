# vim: set tabstop=4
# s_pH_features_on_dataset.py
#!/usr/bin/env python3
""" The script makes plots to classify data based on if the ammonium
oxidation is finished or not (Al Ghusain et al., 1994)."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 05.07.2018

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_pH import aeration_valley

# user
from SBRplots.point_features import (
    feature_vs_value,
    feature_signal_and_output
)
from SBRdata.utils import (
    Result,
    sensorsdict,
    processrawdata,
    fscore,
    contingencytable
)
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# Configure the size and position of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3), loc='lower left',
                 mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])

def plotvalley(*, xval=None, signal=None, output=None, feature=None,
               axes=None, plots=None, case=None, name=None):
    '''
    Function to plot a single cycle, showing date and time of the cycle,
    the original data, the smoothed curve and the measured ammonium efflunet
    concentration, as well as if there has been an ammonium valley found or not.
    If an ammonium valley has been found, the lowest point of the valley is
    marked.

    Arguments
    ---------
    None

    Keyword arguments
    -----------------
    xval : string, optional
        Label of xaxis.
    signal : string, optional
        Label of why axis.
    output : axes, optional
        Subplot of effluent.
    feature : Line2D class, optional
    axes : dictionary, optional
    plots : dictionary, optional
    case : classmethod, optional
        Can process a method that classifies the results.
    name : string, optional
        Name of the cycle.

    Returns
    -------
    '''
    if (axes is None) and (plots is None):
        axes, plots = feature_signal_and_output()
        # Rename singal axes
        axes['signal'].set_xlabel('aeration phase completion [%]')
        axes['signal'].set_ylabel('pH')
        # Rename output axes
        axes['output'].set_ylabel('effluent NH$_{4}^{+}$ nitrogen')
        axes['output'].set_ylim(0, NH4.max())
        # add line to show threshold in output
        plots['outoput_threshold'] = \
            axes['output'].axhline(y=NH4THRESHOLD, xmin=0, xmax=1, c="black",
                                   linewidth=1, linestyle='--')
        handles, _ = axes['signal'].get_legend_handles_labels()
        axes['signal'].legend(handles=handles, **LEGENDFMT)
    else:
        # Update figure
        axes, plots = feature_signal_and_output(
            xval=xval, signal=signal, output=output,
            feature=(feature[0], feature[1]), axes=axes, plots=plots
            )
        plots['signal'].set_label(name.replace('_', r'\_'))

        if feature is None:
            feature = (None, None)
        # Update feature, only time and signal value are used
        # if the feature has more, add it after calling this function
        if feature[0] is not None:
            # draw local slope line
            plots['feature'].set_label('Valley')

        # configure markers according to case
        plots['output'].set_color(case.value.fmt['color'])
        plots['feature'].set(**case.value.fmt)

        # update legend
        handles, _ = axes['signal'].get_legend_handles_labels()
        axes['signal'].legend(handles=handles, **LEGENDFMT)

        print('Showing %s'%name)
        print(case.name)

    return axes, plots

def applyfeature(time, svalue, *, plotfunc=None, sname):
    """
    Applies the feature to the dataset (in this case the pH data of one cycle).
    """
    # Prepare plot
    doplot = False
    if plotfunc is not None:
        doplot = True
        axes, plots = plotfunc()

    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)

    for i, signal in enumerate(svalue): # signal are all pH values of one cycle
        feature_func = {
            'maintained_1': partial(
            aeration_valley, t_interval=timeminmax(time[i][-1]),
            smoother=partial(smooth, order=2, freq=frequencyfkt(time[i][-1]))),
            'unmaintained_1': partial(
            aeration_valley, t_interval=timeminmax(time[i][-1]),
            smoother=partial(smooth, order=2, freq=frequencyfkt(time[i][-1]))),
            'unmaintained_2': partial(
            aeration_valley, t_interval=timeminmax(time[i][-1]),
            smoother=partial(smooth, order=2, freq=frequencyfkt(time[i][-1])))
        }
        tv, sv, ss = feature_func[sname](np.array(time[i]), signal)
        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])
        # If desired, plot the signals
        if doplot:
            axes, plots = plotfunc(
                xval=time[i], signal=(signal, ss), output=NH4[i], feature=(tv, sv),
                axes=axes, plots=plots, case=case[i], name=CYCLENAMES[i]
                )
            plt.pause(0.1) # Hack for thread synchronization in Windows systems
            key = input('Press q and return to quit, return to continue: ')
            if key == 'q':
                quit()
    if doplot:
        plt.close()

    return time_feature, hasfeature, case

def frequencyfkt(length=None):
    """ """
    # import pdb; pdb.set_trace()
    if length is None:
        freq = 2.0
    else:
        freq = 2.0 / length
    return freq

def timeminmax(length=None):
    """ """
    if length is None:
        [0.1, 0.9]
    else:
        t_int_from = 0.1 * length
        t_int_to = 0.99 * length
    return [t_int_from, t_int_to]

if __name__ == '__main__':
    import argparse
    from  functools import partial

    # Parse command line arguments to show plot of every single cycle
    # this means that one can write "python s_pH_features_on_dataset -p" to
    # plot every single cycle.
    parser = argparse.ArgumentParser(description='Run analysis on all pH data.')
    parser.add_argument(
        '-p', '--plot', help='If present, plots each singal in the dataset.',
        action='store_true')
    args = parser.parse_args()

    plotfunc = None
    if args.plot:
        plotfunc = plotvalley

    sensorsource = sensorsdict(stype='pH')
    NH4, nonan, sensor = processrawdata(filesdict=sensorsource)
    NH4ISLOW = (NH4 <= NH4THRESHOLD)

    tminmax = [10, 7000] # time interval in which to look for valley

    # compare prediction with the measured value.
    hasfeature = dict().fromkeys(sensorsource.keys())
    time_feature = dict().fromkeys(sensorsource.keys())
    cases = dict().fromkeys(sensorsource.keys())

    for sname, signal in sensor.items():
        print('** Sensor type: %s' %sname)

        CYCLENAMES = np.array(signal.cycle_names())[nonan]
        ## Filter cycles which do not have an ammonium reference measurement
        ## and select aeration phase (phase 4)
        _time, _sv = signal.select_phase(phase=4)
        _svalue = _sv[nonan, :]
        value = signal.interp_nan(_time, _svalue.copy())
        # time as phase completion 0==start, 1==end
        # time = signal.completion_phase(4)

        svalue = []
        time = []
        i = 0
        for line in value:
             sv = line[0:signal.last_valid_idx[i]]
             st = [10.0*x for x in range(0, signal.last_valid_idx[i])]
             svalue.append(sv)
             time.append(st)
             i += 1

        frequency = 1e-4#1.5e-4


        # time_feature[sname], hasfeature[sname], cases[sname] = applyfeature(
        #     time[sname], svalue, plotfunc=plotfunc, feature=feature_func[sname])
        time_feature[sname], hasfeature[sname], cases[sname] = applyfeature(
            time, svalue, plotfunc=plotfunc, sname=sname)

        TFtable = contingencytable(cases[sname])
        ## Print summary of classification
        for k, v in TFtable.items():
            print('{}:\t{}\t({})'.format(Result[k].value.text, v, k))

        for beta in [0.5, 1, 2]:
            print('F({:.1f})-score: {:.2f}'.format(beta, fscore(TFtable,
                                                                beta**2)))

    masks = dict().fromkeys(sensor.keys())
    for k, v in time_feature.items():
        masks[k] = (cases[k] == Result.FALSE_POSITIVE) \
                 | (cases[k] == Result.FALSE_NEGATIVE)
    phase_completion = {k : v * 100 for k, v in time_feature.items()}

    fig, axes, _ = feature_vs_value(phase_completion, NH4, color_mask=masks)

    axes[0].axvline(x=NH4THRESHOLD, ymax=1.27, c='k', linestyle='--',
                    zorder=0, clip_on=False)
    axes[0].set_xscale('log')
    # axes[0].axis('tight')
    axes[0].set_xlabel('effluent ammonium nitrogen [mg-N/L]')
    axes[0].set_ylabel(r'time [10s]')
    fig.tight_layout(pad=0.1)

    plt.legend(loc="lower right")
    plt.show(block=True)
