# vim: set tabstop=4
# s_pH_features_on_dataset_syntdata.py
#!/usr/bin/env python3
""" The script makes plots to classify data based on if the ammonium
oxidation is finished or not (Al Ghusain et al., 1994). This is evaluate for a
a list of data and the result plotted."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 05.07.2018

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# from sbrfeatures.basicfunctions import smooth
# from sbrfeatures.features_pH import aeration_valley
from sbrfeatures.basicfunctions import smooth
from sbrfeatures.features_pH import aeration_valley

# user
from SBRplots.point_features import (
    feature_vs_value,
    feature_signal_and_output
)
from SBRdata.utils import (
    accuracy,
    contingencytable,
    fscore,
    processrawdata,
    Result,
    sensorsdict,
)
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# Configure the size and position of the legend
LEGENDFMT = dict(bbox_to_anchor=(0, 1.02, 1.3, 0.3), loc='lower left',
                 mode='expand', ncol=3, fontsize='small')

# Globals to pass values to plot functions
NH4 = np.array([])
NH4ISLOW = np.array([])
NH4THRESHOLD = 1
CYCLENAMES = np.array([])

def applyfeature(time, svalue, *, plotfunc=None, sname):
    """
    Applies the feature to the dataset (in this case the pH data of one cycle).
    """
    # Prepare plot
    doplot = False
    if plotfunc is not None:
        doplot = True
        axes, plots = plotfunc()

    # Number of signals
    N = len(svalue)

    # Detect feature and plot
    hasfeature = np.array([False] * N)
    time_feature = np.array([np.nan] * N)
    case = np.array([None] * N)

    for i, signal in enumerate(svalue): # signal are all pH values of one cycle
        tv, sv, ss = aeration_valley(np.array(time[i]), signal,
                                     t_interval=timeminmax(time[i][-1]),
                                     smoother=partial(smooth, order=2,
                                         freq=frequencyfkt(time[i][-1])))

        if tv is not None:
            hasfeature[i] = True
            time_feature[i] = tv
        # Result type
        case[i] = Result.classify(hasfeature[i], NH4ISLOW[i])
        # If desired, plot the signals
        if doplot:
            axes, plots = plotfunc(
                xval=time[i], signal=(signal, ss), output=NH4[i], feature=(tv, sv),
                axes=axes, plots=plots, case=case[i], name=CYCLENAMES[i]
                )
            plt.pause(0.1) # Hack for thread synchronization in Windows systems
            key = input('Press q and return to quit, return to continue: ')
            if key == 'q':
                quit()
    if doplot:
        plt.close()

    return time_feature, hasfeature, case

def frequencyfkt(length=None):
    """ Sets the cutoff frequency parameter depending on the length of the
    cycle. If length is None, the assumption is made that the time is
    normalised in % of cycle passed, so from 0 to 1."""
    if length is None:
        freq = 2.0
    else:
        freq = 2.0 / length
    return freq

def timeminmax(length=None):
    """ Sets the range at the beginning and at the end of a cycle, where a
    local minimum is not considered to be a relevant feature, when falling into
    that range. This margine depends on the length of the cycle.
    If length is None, the assumption is made that the time is normalised in %
    of cycle passed, so from 0 to 1."""
    if length is None:
        [0.1, 0.9]
    else:
        t_int_from = 0.1 * length
        t_int_to = 0.99 * length
    return [t_int_from, t_int_to]

if __name__ == '__main__':
    import argparse
    from  functools import partial

    # Parse command line arguments to show plot of every single cycle
    # this means that one can write "python s_pH_features_on_dataset -p" to
    # plot every single cycle.
    parser = argparse.ArgumentParser(description='Run analysis on all pH data.')
    parser.add_argument(
        '-p', '--plot', help='If present, plots each singal in the dataset.',
        action='store_true')
    args = parser.parse_args()

    plotfunc = None
    if args.plot:
        plotfunc = plotvalley
    DATAPATH = "../data/"
    # time_feature = {}
    # hasfeature = {}
    TFtable = {}
    hasfeature = {}
    time_feature = {}
    cases = {}

    accuracytable = {}

    inputparam = ["SRT_01d", "SRT_02d", "SRT_03d", "SRT_04d", "SRT_05d",
                  "SRT_06d", "SRT_07d", "SRT_08d", "SRT_09d", "SRT_10d",
                  "SRT_11d", "SRT_12d", "SRT_13d", "SRT_14d", "SRT_15d",
                  "SRT_16d", "SRT_17d", "SRT_18d", "SRT_19d",
                  "SRT_20d", "SRT_20d_run"]
    # inputparam = ["SRT20d_modified"]
    for input in inputparam:
        sensorsource = {input : DATAPATH + "su_pH_%s.csv" %input}
        # for secnario, fileloc in sensorsource.items():
        NH4, nonan, scenario = processrawdata(filesdict=sensorsource)
        NH4ISLOW = (NH4 <= NH4THRESHOLD)

        # compare prediction with the measured value.
        for sname, signal in scenario.items():
            print('** Scenario type: %s' %sname)

            CYCLENAMES = np.array(signal.cycle_names())[nonan]
            ## Filter cycles which do not have an ammonium reference measurement
            ## and select aeration phase (phase 4)
            _time, _sv = signal.select_phase(phase=4)
            _svalue = _sv[nonan, :]
            value = signal.interp_nan(_time, _svalue.copy())
            # time as phase completion 0==start, 1==end
            # time = signal.completion_phase(4)

            svalue = []
            time = []
            i = 0
            for line in value:
                 sv = line[0:signal.last_valid_idx[i]]
                 st = [10.0*x for x in range(0, signal.last_valid_idx[i])]
                 svalue.append(sv)
                 time.append(st)
                 i += 1

            # frequency = 2 #1e-4 #1.5e-4

            time_feature[sname], hasfeature[sname], cases[sname] = applyfeature(
                time, svalue, plotfunc=plotfunc, sname=sname)

            TFtable[sname] = contingencytable(cases[sname])
            ## Print summary of classification
            # for k, v in TFtable[sname].items():
            #     print('{}:\t{}\t({})'.format(Result[k].value.text, v, k))
            #
            # for beta in [0.5, 1, 2]:
            #     print('F({:.1f})-score: {:.2f}'.format(beta, fscore(TFtable[sname],
            #                                                         beta**2)))

            print('Accuracy: {:.2f}'.format(accuracy(TFtable[sname])))
            accuracytable[sname] = accuracy(TFtable[sname])
    TFdf = pd.DataFrame(TFtable.items())
    accuracydf = pd.DataFrame(accuracytable.items())
    # df = pd.DataFrame(TFdf, accuracydf)
    df = pd.concat([TFdf, accuracydf], axis=1, sort=False)

    filename = DATAPATH + "result_synthetic_data.csv"
    df.to_csv(filename, sep=";")

    #
    # fig, ax1 = plt.subplots()
    # ax2 = ax1.twinx()
    #
    # for name, value in accuracytable.items():
    #     _SRT = name.split('_')[1]
    #     SRT = int(''.join(s for s in _SRT if s.isdigit()))
    #     ax1.plot(SRT, value, marker='o', markersize=24, c='black',
    #              alpha=0.2, markeredgecolor='black')
    #     for TFname, TFvalue in TFtable[name].items():
    #         if TFname == "TRUE_POSITIVE":
    #             ax2.plot(SRT, TFvalue, marker='^', markersize=10, c='#1f77b4',
    #                      alpha=0.5, markeredgecolor='black')
    #         if TFname == "TRUE_NEGATIVE":
    #             ax2.plot(SRT, TFvalue, marker='^', markersize=10, c='#ff7f0e',
    #                      alpha=0.5, markeredgecolor='black')
    #         if TFname == "FALSE_POSITIVE":
    #             ax2.plot(SRT, TFvalue, marker='.', c='#1f77b4', alpha=0.5,
    #                      markeredgecolor='black')
    #         if TFname == "FALSE_NEGATIVE":
    #             ax2.plot(SRT, TFvalue, marker='.', c='#ff7f0e', alpha=0.5,
    #                      markeredgecolor='black')
    #
    # ax1.set_xlabel('solids retention time (SRT) ins days')
    # ax1.set_ylabel('accuracy [-]')
    # ax2.set_ylabel('Number classified as TP/TN/FP/FN')
    # # fig.legend(["accuracy", "1", "2", "3", "4"])
    # # secaxy.set_ylabel("Number of cycles")
    # plt.show(block=True)

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    i = 0
    for name, value in accuracytable.items():
        _SRT = name.split('_')[1]
        SRT = int(''.join(s for s in _SRT if s.isdigit()))
        alltrue = TFtable[name]["TRUE_POSITIVE"] + TFtable[name]["TRUE_NEGATIVE"]
        allpos = TFtable[name]["TRUE_POSITIVE"] + TFtable[name]["FALSE_NEGATIVE"]
        ax1.plot(SRT, value, marker='o', markersize=7, c='black',
                 alpha=0.2, markeredgecolor='black')

        ax2.plot(SRT, allpos, marker='^', markersize=4, c='#1f77b4',
                 alpha=0.5, markeredgecolor='black')

    ax1.set_xlabel('solids retention time (SRT) ins days')
    ax1.set_ylabel('accuracy [-]')
    ax2.set_ylabel('Number classified as TP/TN/FP/FN')
    # fig.legend(["accuracy", "1", "2", "3", "4"])
    # secaxy.set_ylabel("Number of cycles")
    plt.show(block=True)
